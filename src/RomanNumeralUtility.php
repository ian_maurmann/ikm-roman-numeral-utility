<?php

# MIT License
#
# Copyright (c) 2018, 2019 Ian Maurmann
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

declare(strict_types=1);

namespace ikm\RomanNumeralUtility;

class RomanNumeralUtility implements RomanNumeralUtilityInterface
{
    private $variants;

    private $current_variant_name;

    function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $this->variants             = array();
        $this->current_variant_name = 'standard';
    }


    public function convert(float $given_number)
    {
        $r             = null;
        $variant_name  = $this->current_variant_name;
        $variant       = $this->getVariant($variant_name);
        $max           = $variant->max;
        $is_positive   = (bool) ($given_number > 0);
        $is_negative   = (bool) ($given_number < 0);
        $is_zero       = (bool) ($given_number === 0);
        $is_long       = (bool) ($given_number > $max);
        $thousands     = '';
        $hundreds      = '';
        $tens          = '';
        $ones          = '';
        $converted     = '';




        if($is_positive && !$is_long){
            $thousands = $this->convertPlaceValue($given_number, 1000);
            $hundreds  = $this->convertPlaceValue($given_number, 100);
            $tens      = $this->convertPlaceValue($given_number, 10);
            $ones      = $this->convertPlaceValue($given_number, 1);

            $converted = $thousands . $hundreds . $tens . $ones;
        }

        $r = $converted;

        return $r;

    }



    public function extractDigit(float $given_number, int $place)
    {
        return ($given_number / $place) % 10;
    }

    public function extractPlaceValue(float $given_number, int $place)
    {
        return (($given_number / $place) % 10) * $place;
    }

    public function convertPlaceValue(float $given_number, int $place)
    {
        $variant_name = $this->current_variant_name;
        $variant      = $this->getVariant($variant_name);
        $place_value  = (int) $this->extractPlaceValue($given_number, $place);
        $can_convert  = (bool) (array_key_exists($place_value, $variant->integers)) ? (bool) $variant->integers[$place_value] : false ;
        $converted    = (string) ($can_convert) ? $variant->integers[$place_value] : '' ;

        return $converted;

    }

    private function getVariant($given_variant_name)
    {
        $this->loadVariant($given_variant_name);

        $variant = $this->variants[$given_variant_name];

        return $variant;
    }

    private function loadVariant($given_variant_name)
    {

        if(!array_key_exists($given_variant_name, $this->variants)){
            $this->variants[$given_variant_name] = $this->setupVariant($given_variant_name);
        }

    }



    private function setupVariant($given_variant_name){
        $new_variant = null;

        switch($given_variant_name){
            case 'standard'       : $new_variant = $this->setupStandardRomanNumerals();     break;
            case 'lower-case'     : $new_variant = $this->setupLowerCaseRomanNumerals();    break;
            case 'old-lower-case' : $new_variant = $this->setupOldLowerCaseRomanNumerals(); break;
            case 'medical'        : $new_variant = $this->setupMedicalRomanNumerals();      break;
        }

        return $new_variant;
    }

    private function setupStandardRomanNumerals()
    {
        $new_variant = (object)[];

        $integers = [
            1    => 'I',
            2    => 'II',
            3    => 'III',
            4    => 'IV',
            5    => 'V',
            6    => 'VI',
            7    => 'VII',
            8    => 'VIII',
            9    => 'IX',
            10   => 'X',
            20   => 'XX',
            30   => 'XXX',
            40   => 'XL',
            50   => 'L',
            60   => 'LX',
            70   => 'LXX',
            80   => 'LXXX',
            90   => 'XC',
            100  => 'C',
            200  => 'CC',
            300  => 'CCC',
            400  => 'CD',
            500  => 'D',
            600  => 'DC',
            700  => 'DCC',
            800  => 'DCCC',
            900  => 'CM',
            1000 => 'M',
            2000 => 'MM',
            3000 => 'MMM',
            4000 => null,
            5000 => null,
            6000 => null,
            7000 => null,
            8000 => null,
            9000 => null,
        ];

        $max = 3999;

        $new_variant->integers = $integers;
        $new_variant->max      = $max;

        return $new_variant;
    }

    private function setupLowerCaseRomanNumerals()
    {
        $new_variant = (object)[];

        $integers = [
            1    => 'i',
            2    => 'ii',
            3    => 'iii',
            4    => 'iv',
            5    => 'v',
            6    => 'vi',
            7    => 'vii',
            8    => 'viii',
            9    => 'ix',
            10   => 'x',
            20   => 'xx',
            30   => 'xxx',
            40   => 'xl',
            50   => 'l',
            60   => 'lx',
            70   => 'lxx',
            80   => 'lxxx',
            90   => 'xc',
            100  => 'c',
            200  => 'cc',
            300  => 'ccc',
            400  => 'cd',
            500  => 'd',
            600  => 'dc',
            700  => 'dcc',
            800  => 'dccc',
            900  => 'cm',
            1000 => 'm',
            2000 => 'mm',
            3000 => 'mmm',
            4000 => null,
            5000 => null,
            6000 => null,
            7000 => null,
            8000 => null,
            9000 => null,
        ];

        $max = 3999;

        $new_variant->integers = $integers;
        $new_variant->max      = $max;

        return $new_variant;
    }


    private function setupOldLowerCaseRomanNumerals()
    {
        $new_variant = (object)[];

        $integers = [
            1    => 'j',
            2    => 'ij',
            3    => 'iij',
            4    => 'iv',
            5    => 'v',
            6    => 'vj',
            7    => 'vij',
            8    => 'viij',
            9    => 'ix',
            10   => 'x',
            20   => 'xx',
            30   => 'xxx',
            40   => 'xl',
            50   => 'l',
            60   => 'lx',
            70   => 'lxx',
            80   => 'lxxx',
            90   => 'xc',
            100  => 'c',
            200  => 'cc',
            300  => 'ccc',
            400  => 'cd',
            500  => 'd',
            600  => 'dc',
            700  => 'dcc',
            800  => 'dccc',
            900  => 'cm',
            1000 => 'm',
            2000 => 'mm',
            3000 => 'mmm',
            4000 => null,
            5000 => null,
            6000 => null,
            7000 => null,
            8000 => null,
            9000 => null,
        ];

        $max = 3999;

        $new_variant->integers = $integers;
        $new_variant->max      = $max;

        return $new_variant;
    }



    private function setupMedicalRomanNumerals()
    {
        $new_variant = (object)[];

        $integers = [
            1    => 'j',
            2    => 'ij',
            3    => 'iij',
            4    => 'iv',
            5    => 'v',
            6    => 'vj',
            7    => 'vij',
            8    => 'viij',
            9    => 'ix',
            10   => 'x',
            20   => 'xx',
            30   => 'xxx',
            40   => 'xL',
            50   => 'L',
            60   => 'Lx',
            70   => 'Lxx',
            80   => 'Lxxx',
            90   => 'xC',
            100  => 'C',
            200  => 'CC',
            300  => 'CCC',
            400  => 'CD',
            500  => 'D',
            600  => 'DC',
            700  => 'DCC',
            800  => 'DCCC',
            900  => 'CM',
            1000 => 'M',
            2000 => 'MM',
            3000 => 'MMM',
            4000 => null,
            5000 => null,
            6000 => null,
            7000 => null,
            8000 => null,
            9000 => null,
        ];

        $max = 3999;

        $new_variant->integers = $integers;
        $new_variant->max      = $max;

        return $new_variant;
    }

    public function info()
    {
        return 'ikm/RomanNumeralUtility/RomanNumeralUtility | (c) 2018-2019 Ian Maurmann | MIT License';
    }
}